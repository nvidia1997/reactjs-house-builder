Part 1:Complete the client requirements
-Set up initial project with all of required dependencies 1h
-Create building(house) component which accepts building options 30m
-Create new component (page)(CitiyBuilder) on which to visualize the houses 10m
-Create Building menu 1h and it's redux logic (adding,removing,dublication) 2h
-Persist redux state in localstorage

Part 2:Complete the bonus tasks
-Implement house info modal 30m
-Add some animations/transitions 30m