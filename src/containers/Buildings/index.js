import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Building from '../../components/Building';
import BuildingsMenu from '../../components/BuildingsMenu';
import { createBuildingAction, deleteBuildingAction, changeFloorsCountAction, changeBuildingNameAction, changeBuildingColorAction, dublicateBuildingAction } from './actions';
import { selectBuildings } from './selectors';

import './stylesheets/main.scss';
import './stylesheets/mobile.scss';

class ServerList extends Component {
  /**
   * @param {Array} buildings 
   */
  renderBuildings(buildings) {
    return buildings.map(building => (
        <Building
          building={building}
          key={building.id}
        />
    ));
  }

  render() {
    const {
      buildings,
      createBuilding,
      deleteBuilding,
      changeFloorsCount,
      changeBuildingName,
      changeBuildingColor,
      dublicateBuilding,
    } = this.props;

    return (
      <div className='sections-container'>
        <div className='section section-1'>
          <BuildingsMenu
            buildings={buildings}
            onCreateBuildingClick={createBuilding}
            onDeleteBuildingClick={deleteBuilding}
            onChangeFloorsCount={changeFloorsCount}
            onChangeBuildingName={changeBuildingName}
            onChangeBuildingColor={changeBuildingColor}
            onDublicateBuildingClick={dublicateBuilding}
          />
        </div>
        <div className='section section-2'>
          {this.renderBuildings(buildings)}
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    createBuilding: () => dispatch(createBuildingAction()),
    deleteBuilding: (buildingId) => dispatch(deleteBuildingAction(buildingId)),
    changeFloorsCount: (buildingId, floorsCount) => dispatch(changeFloorsCountAction(buildingId, floorsCount)),
    changeBuildingName: (buildingId, newName) => dispatch(changeBuildingNameAction(buildingId, newName)),
    changeBuildingColor: (buildingId, color) => dispatch(changeBuildingColorAction(buildingId, color)),
    dublicateBuilding: (buildingId) => dispatch(dublicateBuildingAction(buildingId)),
  };
}

const mapStateToProps = (state) => {
  return {
    buildings: selectBuildings(state),
  }
};

ServerList.propTypes = {
  createBuilding: PropTypes.func.isRequired,
  deleteBuilding: PropTypes.func.isRequired,
  changeFloorsCount: PropTypes.func.isRequired,
  changeBuildingName: PropTypes.func.isRequired,
  changeBuildingColor: PropTypes.func.isRequired,
  buildings: PropTypes.array.isRequired,
}

// Wrap the component to inject dispatch and state into it
export default connect(mapStateToProps, mapDispatchToProps)(ServerList);
