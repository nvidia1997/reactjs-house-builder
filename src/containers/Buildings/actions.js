import {
  CREATE_BUILDING,
  BUILDING_CREATED,
  DELETE_BUILDING,
  BUILDING_DELETED,
  CHANGE_FLOORS_COUNT,
  FLOORS_COUNT_CHANGED,
  CHANGE_FLOOR_COLOR,
  FLOOR_COLOR_CHANGED,
  CHANGE_BUILDING_NAME,
  BUILDING_NAME_CHANGED,
  CHANGE_BUILDING_COLOR,
  BUILDING_COLOR_CHANGED,
  DUBLICATE_BUILDING,
} from "./constants"


export const createBuildingAction = () => {
  return {
    type: CREATE_BUILDING,
  }
}

export const buildingCreatedAction = (building) => {
  return {
    type: BUILDING_CREATED,
    payload: building,
  }
}

export const deleteBuildingAction = (buildingId) => {
  return {
    type: DELETE_BUILDING,
    payload: buildingId,
  }
}

export const buildingDeletedAction = (buildingId) => {
  return {
    type: BUILDING_DELETED,
    payload: buildingId,
  }
}

export const changeFloorsCountAction = (buildingId, floorsCount) => {
  return {
    type: CHANGE_FLOORS_COUNT,
    payload: {
      buildingId,
      floorsCount,
    },
  }
}

export const floorsCountChangedAction = (buildingId, floors) => {
  return {
    type: FLOORS_COUNT_CHANGED,
    payload: {
      buildingId,
      floors,
    },
  }
}

export const changeFloorColorAction = (buildingId, floorId, color) => {
  return {
    type: CHANGE_FLOOR_COLOR,
    payload: {
      buildingId,
      floorId,
      color,
    },
  }
}

export const floorColorChangedAction = (buildingId, floorId, color) => {
  return {
    type: FLOOR_COLOR_CHANGED,
    payload: {
      buildingId,
      floorId,
      color,
    },
  }
}

export const changeBuildingColorAction = (buildingId, color) => {
  return {
    type: CHANGE_BUILDING_COLOR,
    payload: {
      buildingId,
      color,
    },
  }
}

export const buildingColorChangedAction = (buildingId, color) => {
  return {
    type: BUILDING_COLOR_CHANGED,
    payload: {
      buildingId,
      color,
    },
  }
}

export const changeBuildingNameAction = (buildingId, newName) => {
  return {
    type: CHANGE_BUILDING_NAME,
    payload: {
      buildingId,
      newName,
    },
  }
}

export const dublicateBuildingAction = (buildingId) => {
  return {
    type: DUBLICATE_BUILDING,
    payload: buildingId,
  }
}

export const buildingNameChangedAction = (buildingId, newName) => {
  return {
    type: BUILDING_NAME_CHANGED,
    payload: {
      buildingId,
      newName,
    },
  }
}