import { createSelector } from 'reselect'
import { NAMESPACE as buildingsStateKey } from './constants';

const MainState = state => state[buildingsStateKey];

export const selectBuildings = createSelector(
  MainState,
  (state) => state.buildings
);