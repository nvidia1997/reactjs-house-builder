import { createLogic } from 'redux-logic';
import uniqid from 'uniqid'
import {
  CREATE_BUILDING,
  DELETE_BUILDING,
  CHANGE_BUILDING_NAME,
  CHANGE_FLOORS_COUNT,
  CHANGE_BUILDING_COLOR,
  DUBLICATE_BUILDING,
} from './constants';
import {
  buildingCreatedAction,
  buildingDeletedAction,
  buildingNameChangedAction,
  floorsCountChangedAction,
  buildingColorChangedAction,
} from './actions';
import { selectBuildings } from './selectors';
import { generateBuilding } from '../../utils/BuildingsHelper';
import {
  MAX_FLOORS_COUNT,
  MIN_FLOORS_COUNT,
} from '../../components/BuildingsMenu/constants';

const createBuildingLogic = createLogic({
  type: CREATE_BUILDING,
  process({ getState, action }, dispatch, done) {
    const building = generateBuilding();

    dispatch(buildingCreatedAction(building));

    done();
  }
});

const deleteBuildingLogic = createLogic({
  type: DELETE_BUILDING,
  process({ getState, action }, dispatch, done) {
    dispatch(buildingDeletedAction(action.payload));

    done();
  }
});

const dublicateBuildingLogic = createLogic({
  type: DUBLICATE_BUILDING,
  process({ getState, action }, dispatch, done) {
    const state = getState();
    const buildings = selectBuildings(state);
    const targetBuilding = buildings.find(b => b.id === action.payload);
    const buildingCopy = {
      ...targetBuilding,
      id: uniqid(),
    };

    dispatch(buildingCreatedAction(buildingCopy));

    done();
  }
});

const changeBuildingNameLogic = createLogic({
  type: CHANGE_BUILDING_NAME,
  process({ getState, action }, dispatch, done) {
    const { buildingId, newName } = action.payload;
    dispatch(buildingNameChangedAction(buildingId, newName));

    done();
  }
});

const changeBuildingColorLogic = createLogic({
  type: CHANGE_BUILDING_COLOR,
  process({ getState, action }, dispatch, done) {
    const { buildingId, color } = action.payload;
    dispatch(buildingColorChangedAction(buildingId, color));

    done();
  }
});

const changeFloorsCountLogic = createLogic({
  type: CHANGE_FLOORS_COUNT,
  process({ getState, action }, dispatch, done) {
    const state = getState();
    const buildings = selectBuildings(state);
    const { buildingId, floorsCount } = action.payload;
    const targetBuilding = buildings.find(b => b.id === buildingId);
    const { floors } = targetBuilding;
    let newFloors;
    if (floorsCount > floors.length) {
      newFloors = [
        ...floors,
        {
          ...floors[floors.length - 1],
          id: uniqid()
        }
      ];
    }
    else {
      newFloors = [...floors];
      newFloors.pop();
    }

    const newFloorsCount = newFloors.length;
    if (newFloorsCount >= MIN_FLOORS_COUNT && newFloorsCount <= MAX_FLOORS_COUNT) {
      dispatch(floorsCountChangedAction(buildingId, newFloors));
    }

    done();
  }
});

export default [
  createBuildingLogic,
  deleteBuildingLogic,
  changeBuildingNameLogic,
  changeFloorsCountLogic,
  changeBuildingColorLogic,
  dublicateBuildingLogic,
];