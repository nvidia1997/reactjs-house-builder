import {
  BUILDING_CREATED,
  BUILDING_DELETED,
  BUILDING_NAME_CHANGED,
  FLOORS_COUNT_CHANGED,
  BUILDING_COLOR_CHANGED,
} from "./constants";


/**
 *  @param {Array<Building>} buildings
 */
const initialState = {
  buildings: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case BUILDING_CREATED:
      return {
        ...state,
        buildings: [...state.buildings, payload],
      };
    case BUILDING_DELETED:
      return {
        ...state,
        buildings: state.buildings.filter((b) => b.id !== payload),
      };
    case BUILDING_NAME_CHANGED:
      return {
        ...state,
        buildings: state.buildings.map(b => (
          b.id === payload.buildingId ? { ...b, name: payload.newName } : b
        ))
      };
    case FLOORS_COUNT_CHANGED:
      return {
        ...state,
        buildings: state.buildings.map(b => (
          b.id === payload.buildingId ? { ...b, floors: payload.floors } : b
        ))
      };
    case BUILDING_COLOR_CHANGED:
      return {
        ...state,
        buildings: state.buildings.map(b => (
          b.id === payload.buildingId ? { ...b, color: payload.color } : b
        ))
      };
    default:
      return state;
  }
}

