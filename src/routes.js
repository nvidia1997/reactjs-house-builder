import React from 'react';
import { Route } from 'react-router';
import Buildings from './containers/Buildings';

export const routePaths = {
  Buildings: '/',
}

export default [
  <Route exact path={routePaths.Buildings} key={routePaths.Buildings} component={Buildings} />,
]