import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';


import BuildingsReducer from './containers/Buildings/reducer.js';
import { NAMESPACE as buildingsReducerKey } from './containers/Buildings/constants';

import { routerReducer } from 'react-router-redux';

const buildingsPersistConfig = {
  key: buildingsReducerKey,
  storage: storage,
}

const rootReducer = combineReducers(
  {
    routing: routerReducer,
    [buildingsReducerKey]: persistReducer(buildingsPersistConfig, BuildingsReducer),
  }
);

export default rootReducer;