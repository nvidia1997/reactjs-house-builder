import uniqid from 'uniqid'
import { COLORS } from '../containers/Buildings/constants';

export const generateBuilding = () => {
  return {
    id: uniqid(),
    name: 'defaultHouseName',
    color: COLORS.RED,
    floors: [
      {
        id: uniqid(),
        color: undefined,
      }
    ]
  };
}
