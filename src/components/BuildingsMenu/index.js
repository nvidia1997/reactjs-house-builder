import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import BuildingOption from './components/BuildingOption';

import './stylesheets/main.scss';

function BuildingsMenu(props) {
  const {
    buildings,
    onCreateBuildingClick,
    onDeleteBuildingClick,
    onChangeFloorsCount,
    onChangeBuildingName,
    onChangeBuildingColor,
    onDublicateBuildingClick,
  } = props;

  const renderOptions = () => {
    return buildings.map((building) => {

      return (
        <BuildingOption
          building={building}
          onDeleteBuildingClick={onDeleteBuildingClick}
          onChangeFloorsCount={onChangeFloorsCount}
          onChangeBuildingName={onChangeBuildingName}
          onChangeBuildingColor={onChangeBuildingColor}
          onDublicateBuildingClick={onDublicateBuildingClick}
          key={`option-${building.id}`}
        />
      );
    });
  }

  return (
    <div className='building-menu-wrapper'>
      <div className='buildings-menu'>
        <div className='header'>Houses List</div>
        {buildings && renderOptions()}
        <div className='footer'>
          <button className='create-building-button' onClick={() => onCreateBuildingClick()}>
            <FontAwesomeIcon icon={faHome} />
            <span>Build a new house</span>
          </button>
        </div>
      </div>
    </div>
  )
}

BuildingsMenu.propTypes = {
  buildings: PropTypes.array.isRequired,
  onCreateBuildingClick: PropTypes.func.isRequired,
  onDeleteBuildingClick: PropTypes.func.isRequired,
  onChangeFloorsCount: PropTypes.func.isRequired,
  onChangeBuildingName: PropTypes.func.isRequired,
  onChangeBuildingColor: PropTypes.func.isRequired,
  onDublicateBuildingClick: PropTypes.func.isRequired,
}

export default BuildingsMenu;
