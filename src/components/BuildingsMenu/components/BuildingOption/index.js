import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faCopy } from '@fortawesome/free-solid-svg-icons'
import { Range } from 'react-range';
import {
  MIN_FLOORS_COUNT,
  MAX_FLOORS_COUNT,
} from '../../constants';
import { COLORS } from '../../../../containers/Buildings/constants';


class BuildingOption extends PureComponent {

  renderOption(building) {
    const {
      id: buildingId,
      color: buildingColor,
      name: buildingName,
      floors,
    } = building;
    const floorsCount = floors.length;

    const {
      onChangeBuildingName,
      onChangeFloorsCount,
      onDeleteBuildingClick,
      onDublicateBuildingClick,
    } = this.props;

    return (
      <div className='building-option'>
        <input type='text'
          className='building-name'
          value={buildingName}
          onChange={(e) => onChangeBuildingName(buildingId, e.target.value)}
        />
        <div className='floors'>
          <span className='label'>Floors:</span>
          <input
            type='number'
            min={MIN_FLOORS_COUNT}
            max={MAX_FLOORS_COUNT}
            className='count'
            value={floorsCount}
            onChange={(e) => onChangeFloorsCount(buildingId, e.target.value)}
          />
        </div>
        <Range
          step={1}
          min={MIN_FLOORS_COUNT}
          max={MAX_FLOORS_COUNT}
          values={[floorsCount]}
          onChange={value => onChangeFloorsCount(buildingId, value)}
          renderTrack={({ props, children }) => (
            <div className='range' {...props}>
              {children}
            </div>
          )}
          renderThumb={({ props }) => (
            <div className='thumb'{...props} />
          )}
        />
        <div className='color'>
          <span className='label'>Color:</span>
          {this.renderColorSelector(buildingId, buildingColor)}
        </div>
        <button className='opt-button delete-building-button' onClick={() => onDeleteBuildingClick(buildingId)}>
          <FontAwesomeIcon icon={faTrash} />
        </button>
        <button className='opt-button dublicate-building-button' onClick={() => onDublicateBuildingClick(buildingId)}>
          <FontAwesomeIcon icon={faCopy} />
        </button>
      </div>
    );
  }

  renderColorSelector(buildingId, buildingColor) {
    const { onChangeBuildingColor } = this.props;

    return (
      <select value={buildingColor} onChange={(e) => onChangeBuildingColor(buildingId, e.target.value)}>
        {Object.keys(COLORS).map(key => {
          var value = COLORS[key];
          return <option value={value} key={value}>{value}</option>
        })}
      </select>
    );
  }

  render() {
    const { building } = this.props;

    return (
      building && this.renderOption(building)
    );
  }
}

BuildingOption.propTypes = {
  building: PropTypes.object.isRequired,
  onDeleteBuildingClick: PropTypes.func.isRequired,
  onChangeFloorsCount: PropTypes.func.isRequired,
  onChangeBuildingName: PropTypes.func.isRequired,
  onChangeBuildingColor: PropTypes.func.isRequired,
  onDublicateBuildingClick: PropTypes.func.isRequired,
}

export default BuildingOption;