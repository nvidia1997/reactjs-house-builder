import React, { PureComponent } from 'react';
import Proptypes from 'prop-types';
import './stylesheets/main.scss';
import Modal from '../Modal';


class Building extends PureComponent {
  state = {
    isHovered: false,
  }

  /**
 * @param {Array} floors 
 */
  renderFloors(floors) {
    return floors.map((floor, floorIndex) => {
      return (
        <div className='floor' key={`floor${floorIndex}`} style={{ backgroundColor: floor.color }}>
          {this.renderWindowsAndDoors(floorIndex, floors.length)}
        </div>)
    });
  }

  renderWindowsAndDoors(floorIndex, floorsCount, windowsCount = 2) {
    const windows = [];
    for (let i = 0; i < windowsCount; i++) {
      let item;
      if (floorIndex + 1 === floorsCount && i === windowsCount - 1) {
        item = <div className='door' key={i} />
      } else {
        item = <div className='window' key={i} />
      }
      windows.push(item);
    }

    return windows;
  }

  setIsHovered(isHovered) {
    this.setState({ isHovered });
  }

  render() {
    const { isHovered } = this.state;
    const { building } = this.props;
    const {
      floors,
      color,
    } = building;

    return (
      <React.Fragment>
        <div className='building' onMouseEnter={() => this.setIsHovered(true)} onMouseLeave={() => this.setIsHovered(false)} >
          <div className='roof'></div>
          <div className='body' style={{ backgroundColor: color }}>
            {floors && this.renderFloors(floors)}
          </div>
        </div>
        {isHovered && <Modal building={building} />}
      </React.Fragment>
    )
  }
}

Building.propTypes = {
  building: Proptypes.object.isRequired,
}

export default Building;
