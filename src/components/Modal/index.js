import React from 'react';
import Proptypes from 'prop-types';
import './stylesheets/main.scss';


function Modal(props) {
  const { building: { id, name, color, floors } } = props;
  const floorsCount = floors.length;

  return (
    <div className='modal' >
      <div><span>Id:</span>{id}</div>
      <div><span>Name:</span>{name}</div>
      <div><span>Color:</span>{color}</div>
      <div><span>Floors count:</span>{floorsCount}</div>
    </div>
  )
}

Modal.propTypes = {
  building: Proptypes.object.isRequired,
}

export default Modal;
