import React from 'react';
import Proptypes from 'prop-types';

import './stylesheets/main.scss'

function Header(props) {
  const { title } = props;

  return (
    <header >
      <div className='title'>{title}</div>
    </header >
  );
}

Header.propTypes = {
  title: Proptypes.string,
}

Header.defaultProps = {
  title: 'City Builder',
}

export default (Header);